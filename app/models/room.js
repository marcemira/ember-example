import DS from 'ember-data';

export default DS.Model.extend({

  name:               DS.attr(),
  mealPlan:           DS.attr(),
  category:           DS.attr(),
  quantityAvailable:  DS.attr('number'),
  isAvailable:        DS.attr('boolean'),

});
