import Ember from 'ember';

export default Ember.Service.extend({

  init(){
    this._super(...arguments);

    // Fixed example data
    this.userToken      = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVkZW50aWFsIjp7InBlcnNvbklkIjo3NjI3ODY1LCJ1c2VySWQiOjk2NzExLCJuYW1lIjoiVElNRSIsImNwZiI6IjQxMDUyOTY0ODUwIiwiYnJhbmNoSWQiOjEwMDAsImFnZW50U2lnbiI6IlciLCJ1c2VyIjoiTVRaQ1BENjAwIn0sInN5c3RlbXMiOlt7InN5c3RlbUNvZGUiOjEsImNsaWVudElEIjo3MTEyOTk0NDY3Mzg4NTkwLCJleHBpcmVzT24iOiIyMDE2LTA3LTA2VDIwOjU4OjQ4LjAwMC0wMzowMCIsImFjdGl2ZSI6IlMgIn0seyJzeXN0ZW1Db2RlIjozLCJjbGllbnRJRCI6NzExMjk5NDQ2NzM4ODU5MSwiZXhwaXJlc09uIjoiMjAxNi0wNy0wNlQyMDoxMzo0OC4wMDAtMDM6MDAiLCJhY3RpdmUiOiJTICJ9XSwiaWF0IjoxNDY3ODQ1OTI2fQ.UJ-ijCm_fjTgSwoTQ4JgVn4hyQrQYWQfFIWzJZUwCT0";
    this.transactionId  = "1234567890";
  }

});
