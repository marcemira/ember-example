import DS from 'ember-data';

export default DS.RESTSerializer.extend({

  normalizeQueryRecordResponse(store, primaryModelClass, hash, id, requestType) {
    
    let payload = {
      room: {
        id:                 hash.room.rates[0].rateToken,
        name:               hash.room.name,
        mealPlan:           hash.room.mealPlan,
        quantityAvailable:  hash.room.quantityAvailable,
        category:           hash.room.category,
        isAvailable:        hash.room.isAvailable
      }
    };

    return this._super(store, primaryModelClass, payload, id, requestType);
  }

});
