import Ember from 'ember';
import DS from 'ember-data';
import ENV from '../config/environment';
import UrlTemplates from "ember-data-url-templates";

export default DS.RESTAdapter.extend(UrlTemplates, {

  host: ENV.API.host,
  namespace: ENV.API.namespace,

  session: Ember.inject.service(),

  headers: Ember.computed('session.userToken', 'session.transactionId', function() {
    return {
      "Gtw-Sec-User-Token": this.get("session.userToken"),
      "Gtw-Transaction-Id": this.get('session.transactionId')
    };
  })

});
