import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({

  queryRecordUrlTemplate: '{+host}{/namespace}/hotels{/hotel_id}/rooms{/room_id}'

});
