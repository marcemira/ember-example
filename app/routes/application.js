import Ember from 'ember';

export default Ember.Route.extend({

  actions: {

    findRoom(hotelId, roomId){
      if( !hotelId || !roomId ){
        let controller = this.controllerFor('application');
        controller.set('validationError', true);
        this.transitionTo('index');
      } else {
        this.transitionTo('room', hotelId, roomId);
      }

      return false;
    },

    exampleFill(){
      let controller = this.controllerFor('application');

      controller.set('hotelId', "36124539");
      controller.set('roomId', "PGhvdDpyYXRlVG9rZW4geG1sbnM6aG90PSJodHRwOi8vY3ZjLmNvbS5ici9tb2RlbC9ob3RlbHMiIHBsYT0iMzYiIGNtaT0iMzYiIGNtYz0iMzYwMDEiIHJ0az0iZDM5MWEyNzktMzlkNC00MDAxLThjYzctMjMzZmEzM2ZmOWFlLTUwMDMiIHJ0cD0iNDgyMDQwIiBydGM9IjIwNDc3ODc1NSIgY2hhPSI3NzUuNjAiIG5hZD0iMSIgYmV0PSIxMyIgY3VyPSJCUkwiIGl6bz0iMzM4MzAiIGFncz0iVyIgYnJjPSIxMDAwIiBsYW49InB0X0JSIiBkaW49IjIwMTYtMTAtMjgiIGRvdT0iMjAxNi0xMC0yOSIgcGtnPSJWSEkiIHB4cz0iMzAiLz4=");

      return false;
    },

    closeMsg(){
      let controller = this.controllerFor('application');
      controller.set('validationError', false);

      return false;
    }

  }

});
