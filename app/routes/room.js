import Ember from 'ember';

export default Ember.Route.extend({

  model({ hotel_id, room_id }){
    return this.store.queryRecord('room', { hotel_id, room_id });
  },

});
