export default function() {

  const payload = JSON.parse(`{"room":{"name":"STANDARD CASAL DIÁRIA(S) COM CAFÉ DA MANHÃ","mealPlan":"DIÁRIA(S) COM CAFÉ DA MANHÃ","category":"STANDARD CASAL","minPax":1,"maxPax":3,"quantityAvailable":5,"isAvailable":true,"rates":[{"packageGroup":"STANDALONE","rateToken":"PHJhdGVUb2tlbiBhZ3M9IlciIGJyYz0iMTAwMCIgY2F0PSIxNDcwIiBjbWM9IjQ1MDAxIiBjbWk9IjQ1IiBjdXI9IkJSTCIgZGluPSIyMDE2LTEwLTI4IiBkb3U9IjIwMTYtMTAtMjkiIGRzYz0iU1RBTkRBUkQgQ0FTQUwiIGZlZT0iMCIgaG90PSI0NTY1OTA3MjQiIGl6bz0iOTYyNiIgbGFuPSJwdF9CUiIgbG9kPSJIb3RlbCIgbWVsPSJEScOBUklBKFMpIENPTSBDQUbDiSBEQSBNQU5Iw4MiIG1rcD0iMC42MzQ4IiBwa2c9IlNUQU5EQUxPTkUiIHBsYT0iNDUiIHByYz0iMTgxIiBweHM9IjMwIiByZXQ9IiIgcm9tPSIyMDgzMjAiLz4=","currency":"BRL","priceWithTax":285.13,"priceWithoutTax":285.13,"pricePerDayWithTax":285.13,"pricePerDayWithoutTax":285.13,"contents":[{"code":"description-agreement","description":"Description Agreement","items":[{"code":"description-agreement","description":"DIÁRIA(S) COM CAFÉ DA MANHÃ (APARTAMENTO COM 1 CAMA DE CASAL + 1 SOFÁ CAMA) - BRASIL FÁCIL"}]}]}]}}`);

  this.get('/hotels/:hotel_id/rooms/:room_id', () => payload );
  
}
