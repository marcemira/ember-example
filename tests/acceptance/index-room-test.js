import { test } from 'qunit';
import moduleForAcceptance from 'ember-example/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | index room');

const hotel_id  = "36124539",
      room_id   = 'PGhvdDpyYXRlVG9rZW4geG1sbnM6aG90PSJodHRwOi8vY3ZjLmNvbS5ici9tb2RlbC9ob3RlbHMiIHBsYT0iMzYiIGNtaT0iMzYiIGNtYz0iMzYwMDEiIHJ0az0iZDM5MWEyNzktMzlkNC00MDAxLThjYzctMjMzZmEzM2ZmOWFlLTUwMDMiIHJ0cD0iNDgyMDQwIiBydGM9IjIwNDc3ODc1NSIgY2hhPSI3NzUuNjAiIG5hZD0iMSIgYmV0PSIxMyIgY3VyPSJCUkwiIGl6bz0iMzM4MzAiIGFncz0iVyIgYnJjPSIxMDAwIiBsYW49InB0X0JSIiBkaW49IjIwMTYtMTAtMjgiIGRvdT0iMjAxNi0xMC0yOSIgcGtnPSJWSEkiIHB4cz0iMzAiLz4=',
      payload   = JSON.parse(`{"room":{"name":"STANDARD CASAL DIÁRIA(S) COM CAFÉ DA MANHÃ","mealPlan":"DIÁRIA(S) COM CAFÉ DA MANHÃ","category":"STANDARD CASAL","minPax":1,"maxPax":3,"quantityAvailable":5,"isAvailable":true,"rates":[{"packageGroup":"STANDALONE","rateToken":"PHJhdGVUb2tlbiBhZ3M9IlciIGJyYz0iMTAwMCIgY2F0PSIxNDcwIiBjbWM9IjQ1MDAxIiBjbWk9IjQ1IiBjdXI9IkJSTCIgZGluPSIyMDE2LTEwLTI4IiBkb3U9IjIwMTYtMTAtMjkiIGRzYz0iU1RBTkRBUkQgQ0FTQUwiIGZlZT0iMCIgaG90PSI0NTY1OTA3MjQiIGl6bz0iOTYyNiIgbGFuPSJwdF9CUiIgbG9kPSJIb3RlbCIgbWVsPSJEScOBUklBKFMpIENPTSBDQUbDiSBEQSBNQU5Iw4MiIG1rcD0iMC42MzQ4IiBwa2c9IlNUQU5EQUxPTkUiIHBsYT0iNDUiIHByYz0iMTgxIiBweHM9IjMwIiByZXQ9IiIgcm9tPSIyMDgzMjAiLz4=","currency":"BRL","priceWithTax":285.13,"priceWithoutTax":285.13,"pricePerDayWithTax":285.13,"pricePerDayWithoutTax":285.13,"contents":[{"code":"description-agreement","description":"Description Agreement","items":[{"code":"description-agreement","description":"DIÁRIA(S) COM CAFÉ DA MANHÃ (APARTAMENTO COM 1 CAMA DE CASAL + 1 SOFÁ CAMA) - BRASIL FÁCIL"}]}]}]}}`);

test('visiting / - filling-in form - clicks find - transitions to /room', function(assert) {
  visit('/');

  fillIn('#hotel_id', hotel_id);
  fillIn('#room_id', room_id);

  click('#find_room_btn');

  andThen(() => {
    assert.equal(currentURL(), `/room/${hotel_id}/${room_id}`);
  });
});

test('visiting /room/:hotel_id/:room_id - room data displays ok', function(assert) {
  visit(`/room/${hotel_id}/${room_id}`);

  andThen(() => {
    assert.equal(find('#room_name').text(), payload.room.name);
    assert.equal(find('#room_plan').text(), payload.room.mealPlan);
    assert.equal(find('#room_category').text(), payload.room.category);
    assert.equal(find('#room_available').text(), payload.room.quantityAvailable);
    assert.equal(find('#room_is_available').text() === "yes" ? true : false, payload.room.isAvailable);
  });
});
